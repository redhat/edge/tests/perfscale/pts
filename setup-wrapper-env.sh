#!/bin/bash
# Setup a python virutal enviornment for benchmark wrapper. Assumes CentOS and pyython3.8

python3.8 -m venv bw-env
git clone https://github.com/cloud-bulldozer/benchmark-wrapper.git bw-env/benchmark-wrapper
source ./bw-env/bin/activate
pip install --upgrade pip
pip install ./bw-env/benchmark-wrapper/
