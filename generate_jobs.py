#!/usr/bin/env python3

from ruamel.yaml import YAML
from pprint import pprint

def create_job(sut, test, workload):

    if  workload['platform'] == "baremetal":
        platform_type = ''
    else:
        workload['platform'] = "container"
        platform_type = '-c'
    # else:
    #     print("Invalid platform: container or baremetal'")
    #     sys.exit(1)


    job_name = sut.lower() + '-' + workload['platform'] + ':' + test['name'] + '_' + workload['name']
    # print(job_name)

    benchmark_args = ""
    for arg_key in workload['args']:
        benchmark_args += f'--{arg_key}={workload["args"][arg_key]} '
    benchmark_args = benchmark_args.strip()

    job_def = {
            'extends': [
                f'.{test["tool"]}',
                '.platform:' + sut.lower()
                ],
            'variables': {
                'PLATFORM_TYPE': platform_type,
                'BENCHMARK_ARGS': benchmark_args,
                'GENERATOR_WRAPPER_ARGS':  f'--sample={workload["sample"]}'
                }
            }

    return (job_name, job_def)

def get_suts(default_suts, workload):
    suts = default_suts
    if 'suts' in workload:
        suts = workload['suts']

    return suts

def get_sample(default_sample, workload):
    sample = default_sample
    if 'sample' in workload:
        sample = workload['sample']

    return sample

def get_args(default_args, workload):
    args = default_args
    if 'args' in workload:
        args.update(workload['args'])

    return args

def get_platform(default_platform, workload):
    platform = default_platform
    if 'platform' in workload:
        platform = workload['platform']

    return platform



def main(argv):
    yaml = YAML()
    if len(argv) != 2:
        raise SystemExit(f'Usage: {argv[0]} ' 'test_spec [control_node]')

    filename = argv[1]
    control_node = ''
    if len(argv) == 3:
        control_node = argv[2]

    spec = {}
    with open(filename, "r") as spec_file:
        spec = yaml.load(spec_file)


    generated_jobs = {}

    ## One job that submits all the archive files
    #generated_jobs['es_submit'] = {
            #		'extends': [
                #			'.es_submit'
                #		],
            #}

    for test in spec['tests']:
        workloads = test.pop('workloads')
        for workload in workloads:

            workload['suts'] = get_suts(test['default_suts'], workload)
            workload['args'] = get_args(test['default_args'], workload)
            workload['sample'] = get_sample(test['default_sample'], workload)
            workload['platform'] = get_platform(test['default_platform'], workload)

            for sut in workload['suts']:
                (job_name, job_def) = create_job(sut, test, workload)
                generated_jobs[job_name] = job_def
                generated_jobs['es_submit:' + job_name ] = {
                        'extends': [ '.es_submit' ],
                        'needs': [ job_name ],
                        }

    with open('generated_jobs.yml', 'w') as outfile:
        yaml.default_flow_style = False
        yaml.dump(generated_jobs, outfile)

if __name__ == '__main__':
    import sys
    main(sys.argv)
