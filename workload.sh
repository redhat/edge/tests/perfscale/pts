#!/bin/bash


usage() {
	echo "$0 [-c] <tool name> <workload config file path> <added benchmark wrapper options>" 1>&2
	echo " -c Run in a container, otherwise default to baremetal" 1>&2
	exit 1;
}
# Default to sysbench and baremetal
CMD="" 
ENVIRONMENT="baremetal"

DATE=$(date -u +"%Y%m%dT%H%M%S")

while getopts "bc" o; do
  case "${o}" in
    b)
      echo" Default to baremetal"
      ;;
    c)
      ENVIRONMENT="container"
      ;;
    *)
      usage
      ;;
  esac
done
shift $((OPTIND-1))
		
if [ "$#" -lt 1 ]; then
  usage
fi

echo "Running $CMD $@ on $ENVIRONMENT"
TOOL=$1
shift

FILENAME=$(hostnamectl hostname)_${TOOL}_${ENVIRONMENT}_${DATE}

echo "PTS_FILENAME=$FILENAME" >> pts-run.env
BENCHMARK_WRAPPER_ARGS=$@
echo "Added benchmark wrapper opts:" $BENCHMARK_WRAPPER_ARGS


if [ "$ENVIRONMENT" = "container" ]; then
  CMD="podman run -v $(pwd)/:/root/ --security-opt label:disable -w /root --rm quay.io/cloud-bulldozer/$(echo $TOOL | tr -d '-') "
fi

# For benchmark-wrapper appending to elastic search the os name and version
. /etc/os-release
clustername=${clustername}-${ID}${VERSION_ID}

START_TIME=$(date -u +"%Y-%m-%dT%H:%M:%SZ")
echo "PTS_START_TIME=\"$START_TIME"\" >> pts-run.env

# Run pcp logger
mkdir _pcp/
if command -v pmlogger > /dev/null 2>&1; then 
  pmlogger -c ./pts/pmlogger.conf -t 1 ./_pcp/${FILENAME} &
else 
  echo "PCP not installed, skipping collection of system metrics...";
fi;


# Sleep 3 seconds prior to start 
sleep 3

# For some reason I need an extra command before running benchmark-wrapper when using a container, otherwise it doesn't use the virutalenv
eval "${CMD} run_snafu --tool ${TOOL}  --create-archive --archive-file $FILENAME.archive $BENCHMARK_WRAPPER_ARGS"

# Sleep 3 seconds to have system calm down
sleep 3

# Stop pcp logger
pkill -2 pmlogger

END_TIME=$(date -u +"%Y-%m-%dT%H:%M:%SZ")
echo "PTS_END_TIME=\"$END_TIME"\" >> pts-run.env

echo "start_time: ${START_TIME}" >> $FILENAME.log
echo "end_time: ${END_TIME}" >> $FILENAME.log
echo "workload_args: $@" >> $FILENAME.log
echo "Complete command line: $CMD $@" >> $FILENAME.log
echo "===============================================" >> $FILENAME.log
